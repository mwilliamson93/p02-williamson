//
//  ViewController.m
//  p02-williamson
//
//  Created by Matthew Williamson on 1/29/16.
//  Copyright © 2016 Matthew Williamson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UISwipeGestureRecognizer * swipeUp =[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeUp:)];
    swipeUp.direction=UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:swipeUp];
    
    UISwipeGestureRecognizer * swipeLeft =[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
    swipeLeft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeft];
    
    UISwipeGestureRecognizer * swipeRight =[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)];
    swipeRight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRight];
    
    UISwipeGestureRecognizer * swipeDown =[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeDown:)];
    swipeDown.direction=UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipeDown];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@synthesize t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15, score, best, title;
@synthesize up, left, right, down, reset;

NSArray *tiles;
int tileValues[4][4];
int scoreValue;
int bestValue;
int endFlag = 0;
int winFlag = 0;

-(void)swipeUp:(UISwipeGestureRecognizer*)gestureRecognizer {
    [self stepInGame:1 ie:4 ii:1 iu:-1 js:0 je:4 ji:1 ju:0];
}

-(void)swipeLeft:(UISwipeGestureRecognizer*)gestureRecognizer {
    [self stepInGame:0 ie:4 ii:1 iu:0 js:1 je:4 ji:1 ju:-1];
}

-(void)swipeRight:(UISwipeGestureRecognizer*)gestureRecognizer {
    [self stepInGame:0 ie:4 ii:1 iu:0 js:2 je:-1 ji:-1 ju:1];
}

-(void)swipeDown:(UISwipeGestureRecognizer*)gestureRecognizer {
    [self stepInGame:2 ie:-1 ii:-1 iu:1 js:0 je:4 ji:1 ju:0];
}

- (IBAction)upClick:(id)sender {
    [self stepInGame:1 ie:4 ii:1 iu:-1 js:0 je:4 ji:1 ju:0];
}

- (IBAction)leftClick:(id)sender {
    [self stepInGame:0 ie:4 ii:1 iu:0 js:1 je:4 ji:1 ju:-1];
}

- (IBAction)rightClick:(id)sender {
    [self stepInGame:0 ie:4 ii:1 iu:0 js:2 je:-1 ji:-1 ju:1];
}

- (IBAction)downClick:(id)sender {
    [self stepInGame:2 ie:-1 ii:-1 iu:1 js:0 je:4 ji:1 ju:0];
}

- (void)stepInGame:(int)is ie:(int)ie ii:(int)ii iu:(int)iu js:(int)js je:(int)je ji:(int)ji ju:(int)ju {
    if (endFlag == 1) {
        return;
    }
    int i;

#define Iterate(a)              \
i = is;                         \
while (i != ie) {               \
int j = js;                     \
while (j != je) {               \
a;                              \
j += ji;                        \
}                               \
i += ii;                        \
}
    
    Iterate([self move:i y1:j x2:(i+iu) y2:(j+ju)])
    Iterate([self move:i y1:j x2:(i+iu) y2:(j+ju)])
    Iterate([self move:i y1:j x2:(i+iu) y2:(j+ju)])
    Iterate([self merge:i y1:j x2:(i+iu) y2:(j+ju)])
    Iterate([self move:i y1:j x2:(i+iu) y2:(j+ju)])
#undef Iterate
    [self updateLabels];
    if (endFlag == 1) {
        [title setText:@"Game Over"];
    }
    if (winFlag == 1) {
        [title setText:@"You Win!"];
        endFlag = 1;
    }
}

- (IBAction)resetClick:(id)sender {
    winFlag = 0;
    [title setText:@"2048 Clone"];
    [reset setTitle:@"Reset" forState:UIControlStateNormal];
    scoreValue = 0;
    
    tiles = @[
        @[[self t0], [self t1], [self t2], [self t3]],
        @[[self t4], [self t5], [self t6], [self t7]],
        @[[self t8], [self t9], [self t10], [self t11]],
        @[[self t12], [self t13], [self t14], [self t15]]
         ];
    
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            tileValues[i][j] = 0;
        }
    }
    
    [self randUpdate];
    [self updateLabels];
}

- (void)move:(int)x1 y1:(int)y1 x2:(int)x2 y2:(int)y2 {
    if (tileValues[x2][y2] == 0) {
        tileValues[x2][y2] = tileValues[x1][y1];
        tileValues[x1][y1] = 0;
    }
}

- (void)merge:(int)x1 y1:(int)y1 x2:(int)x2 y2:(int)y2 {
    if (tileValues[x1][y1] == tileValues[x2][y2]) {
        tileValues[x2][y2] *= 2;
        tileValues[x1][y1] = 0;
        scoreValue += tileValues[x2][y2];
        if (scoreValue > bestValue) {
            bestValue = scoreValue;
        }
        
        if (tileValues[x2][y2] == 2048) {
            winFlag = 1;
        }
    }
}

- (void)randUpdate {
    int i;
    int j;
    endFlag = 1;
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            if (tileValues[i][j] == 0) {
                endFlag = 0;
            }
        }
    }
    if (endFlag == 1) {
        return;
    }
    do {
        i = arc4random_uniform(4);
        j = arc4random_uniform(4);
    } while (tileValues[i][j] != 0);
    
    int c = arc4random_uniform(10);
    if (c == 0) {
        c = 4;
    }
    else {
        c = 2;
    }
    tileValues[i][j] = c;
}

- (void) updateLabels {
    [self randUpdate];
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            if (tileValues[i][j] == 0) {
                [tiles[i][j] setText:@""];
                [(UILabel *)tiles[i][j] setBackgroundColor:[UIColor grayColor]];
            }
            else {
                [(UILabel *)tiles[i][j] setText:[@(tileValues[i][j]) stringValue]];
                if (tileValues[i][j] == 2) {
                    [(UILabel *)tiles[i][j] setBackgroundColor:[UIColor lightGrayColor]];
                }
                else if (tileValues[i][j] == 4) {
                    [(UILabel *)tiles[i][j] setBackgroundColor:[UIColor blueColor]];
                }
                else if (tileValues[i][j] == 8) {
                    [(UILabel *)tiles[i][j] setBackgroundColor:[UIColor greenColor]];
                }
                else if (tileValues[i][j] == 16) {
                    [(UILabel *)tiles[i][j] setBackgroundColor:[UIColor orangeColor]];
                }
                else if (tileValues[i][j] == 32) {
                    [(UILabel *)tiles[i][j] setBackgroundColor:[UIColor cyanColor]];
                }
                else if (tileValues[i][j] == 64) {
                    [(UILabel *)tiles[i][j] setBackgroundColor:[UIColor magentaColor]];
                }
                else if (tileValues[i][j] == 128) {
                    [(UILabel *)tiles[i][j] setBackgroundColor:[UIColor yellowColor]];
                }
                else if (tileValues[i][j] == 256) {
                    [(UILabel *)tiles[i][j] setBackgroundColor:[UIColor brownColor]];
                }
                else if (tileValues[i][j] == 512) {
                    [(UILabel *)tiles[i][j] setBackgroundColor:[UIColor purpleColor]];
                }
                else if (tileValues[i][j] == 1024) {
                    [(UILabel *)tiles[i][j] setBackgroundColor:[UIColor colorWithRed:0.76 green:0.48 blue:0.63 alpha:1.0]];
                }
                else if (tileValues[i][j] == 2048) {
                    [(UILabel *)tiles[i][j] setBackgroundColor:[UIColor colorWithRed:0.29 green:0.53 blue:0.91 alpha:1.0]];
                }
            }
        }
    }
    
    [score setText:[@(scoreValue) stringValue]];
    [best setText:[@(bestValue) stringValue]];
}

@end
